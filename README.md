# README #


* Virtual Piano based on Kinect, OpenNI, OpenCV and irrKlang

To use, 

* install OpenCV library http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html

* install OpenNI library and sensor modules for kinect
http://mitchtech.net/ubuntu-kinect-openni-primesense/

* install irrKlang sound library
http://www.ambiera.com/irrklang/

*maketrial.txt contains the compilation code with g++ . Modify the paths to suit your purposes. 