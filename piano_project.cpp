//Author: R Santhosh Kumar
//Institute: IIT Madras
//Contact: ee12b101@ee.iitm.ac.in

//---------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------
#include <XnOS.h>
#if (XN_PLATFORM == XN_PLATFORM_MACOSX)
	#include <GLUT/glut.h>
#else
	#include <GL/glut.h>
#endif
#include <math.h>
#include <irrKlang.h>
#include <XnCppWrapper.h>
#include "cv.h"
#include "highgui.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include "opencv2/imgproc/imgproc.hpp"

#if defined(WIN32)
#include <conio.h>
#else
#include "/home/obiwan/Desktop/irrKlang-1.4.0b/irrKlang-1.4.0/examples/common/conio.h"
#endif

using namespace std;
using namespace cv;
using namespace xn;
using namespace irrklang;

#pragma comment(lib, "irrKlang.lib") // link with irrKlang.dll

//---------------------------------------------------------------------------
// Defines
//---------------------------------------------------------------------------
#define SAMPLE_XML_PATH "../../../../Data/SamplesConfig.xml"

#define GL_WIN_SIZE_X 1280
#define GL_WIN_SIZE_Y 1024

#define DISPLAY_MODE_OVERLAY	1
#define DISPLAY_MODE_DEPTH		2
#define DISPLAY_MODE_IMAGE		3
#define DEFAULT_DISPLAY_MODE	DISPLAY_MODE_DEPTH
#define threshold_value 500
#define MAX_DEPTH 10000

//---------------------------------------------------------------------------
// Globals
//---------------------------------------------------------------------------
float g_pDepthHist[MAX_DEPTH];
uint16_t t_gamma[2048];
float distance[2048];
int inv_t_gamma[10000];

XnRGB24Pixel* g_pTexMap = NULL;
XnRGB24Pixel* g_pTexMap1 = NULL;
unsigned int g_nTexMapX = 0;
unsigned int g_nTexMapY = 0;

unsigned int g_nViewState = DEFAULT_DISPLAY_MODE;

Context g_context;
ScriptNode g_scriptNode;
DepthGenerator g_depth;
ImageGenerator g_image;
DepthMetaData g_depthMD;
ImageMetaData g_imageMD;

//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------


VideoWriter writer1("/home/obiwan/Desktop/Piano project/piano_rgb2.avi", CV_FOURCC('D', 'I', 'V', 'X'), 30, Size(640, 480));
VideoWriter writer2("/home/obiwan/Desktop/Piano project/piano_depth2.avi", CV_FOURCC('D', 'I', 'V', 'X'), 30, Size(640, 480));

Point2f p[4],point;
int cnt = 0;
Mat img, img2;
Mat img_canny;
Mat img_grayscale, img2_grayscale;
Mat img_xor_erode;

void on_mouse( int event, int x, int y, int d, void *ptr )
{
    switch( event )
    {
    	case CV_EVENT_LBUTTONUP:
    		p[cnt] = Point(x,y);
	    	cnt++;
	    	printf("X: %d, Y: %d\n", x, y);
	    	if(cnt == 4)
	    		//destroyWindow("Image");
	    	break;
    }
}

void quickSort(int arr[], int left, int right) {
      int i = left, j = right;
      int tmp;
      int pivot = arr[(left + right) / 2];
 
      /* partition */
      while (i <= j) {
            while (arr[i] < pivot)
                  i++;
            while (arr[j] > pivot)
                  j--;
            if (i <= j) {
                  tmp = arr[i];
                  arr[i] = arr[j];
                  arr[j] = tmp;
                  i++;
                  j--;
            }
      };
 
      /* recursion */
      if (left < j)
            quickSort(arr, left, j);
      if (i < right)
            quickSort(arr, i, right);
}

int pixel_depth(Vec3b colour){
	uint16_t val=0;
	uint16_t d;
	uint8_t lb;
	uint8_t ub;
	if(colour[2]==255){
		if(colour[0]==0){
			if(colour[1]==0){
				ub=1;
				lb=0;
			}
			else if(colour[1]==255){
				lb=0;
				ub=2;
			}
			else
			{
				ub=0;
				lb=255-colour[1];
			}
		}
		else if(colour[0]==255){
			ub=0;
			lb=0;
		}
		else
		{
			ub=0;
			lb=255-colour[1];
		}
	}
	else if(colour[2]==0){
		if(colour[1]==255){
			if(colour[0]==255){
				ub=4;
				lb=0;
			}
			else if(colour[0]==0){
				ub=3;
				lb=0;
			}
			else 
			{
				ub = 3;
				lb = colour[0];
			}
				
		}
		else if(colour[1]==0){
			if(colour[0]==255){
				ub=5;
				lb=0;
			}
			else if(colour[0]==0){
				ub=6;
				lb=0;
			}
			else
			{
				ub=5;
				lb=255-colour[1];
			}
		}
		else
		{
			ub=4;
			lb=255-colour[1];
		}
	}
	else
	{
		ub=2;
		lb=255-colour[2];
	}
	val=ub;
	val=val<<8;
	val=val+lb;
	d=inv_t_gamma[val];
	return d;
}

void glutIdle (void)
{
	// Display the frame
	glutPostRedisplay();
}
Mat convertImageToCVImage(const XnRGB24Pixel* imageRow, const ImageMetaData& metaData) { 
        cv::Mat colorArray[3]; 
        cv::Mat colorImage; 
        const XnRGB24Pixel* pixel; 
        colorArray[0] = cv::Mat(metaData.YRes(), metaData.XRes(), CV_8U); 
        colorArray[1] = cv::Mat(metaData.YRes(), metaData.XRes(), CV_8U); 
        colorArray[2] = cv::Mat(metaData.YRes(), metaData.XRes(), CV_8U); 
        for (int i = 0; i < metaData.YRes(); i++) { 
                pixel = imageRow; 
                uchar* blue = colorArray[0].ptr<uchar>(i); 
                uchar* green = colorArray[1].ptr<uchar>(i); 
                uchar* red = colorArray[2].ptr<uchar>(i); 
                for (int j = 0; j < metaData.XRes(); ++j, ++pixel) { 
                        blue[j] = pixel->nBlue; 
                        green[j] = pixel->nGreen; 
                        red[j] = pixel->nRed; 
                } 
                imageRow += metaData.XRes(); 
        } 
        cv:: merge(colorArray, 3, colorImage); 
        return colorImage; 
} 

int InOrOut( Rect a, Point p)
{
	if( (p.x >= a.x) && (p.x <= a.x + a.width) && (p.y >= a.y) && (p.y <= a.y + a.height) )
		return 1;
	else return 0;
}  

Mat run_code(void)
{
	XnStatus rc = XN_STATUS_OK;
	// Read a new frame
	rc = g_context.WaitAnyUpdateAll();
	/*if (rc != XN_STATUS_OK)
	{
		printf("Read failed: %s\n", xnGetStatusString(rc));
		return;
	}*/
	Mat image(480,640,CV_16UC1);
	g_depth.GetAlternativeViewPointCap().SetViewPoint(g_image);
	g_depth.GetMetaData(g_depthMD);
	g_image.GetMetaData(g_imageMD);
	const XnDepthPixel* pDepth = g_depthMD.Data(); // this is the array of depth values
	const XnUInt8* pImage = g_imageMD.Data();
	unsigned int nImageScale = GL_WIN_SIZE_X / g_depthMD.FullXRes();
	// Calculate the accumulative histogram (the yellow display...)
	xnOSMemSet(g_pDepthHist, 0, MAX_DEPTH*sizeof(float));
	
	unsigned int nNumberOfPoints = 0;
	for (XnUInt y = 0; y < g_depthMD.YRes(); ++y)
	{
		for (XnUInt x = 0; x < g_depthMD.XRes(); ++x, ++pDepth)
		{
			if (*pDepth != 0)
			{
				uint16_t d = (uint16_t)(*pDepth);
				//printf("\n");
				image.at<unsigned short>(y, x) = 32 * d;
			}
		}
	}
	
	xnOSMemSet(g_pTexMap, 0, g_nTexMapX*g_nTexMapY*sizeof(XnRGB24Pixel));
	const XnDepthPixel* pDepthRow = g_depthMD.Data();
	XnRGB24Pixel* pTexRow = g_pTexMap + g_depthMD.YOffset() * g_nTexMapX;
	
	pDepthRow = g_depthMD.Data();
	pTexRow = g_pTexMap + g_depthMD.YOffset() * g_nTexMapX;
	for (XnUInt y = 0; y < g_depthMD.YRes(); ++y)
	{
		const XnDepthPixel* pDepth = pDepthRow;
		XnRGB24Pixel* pTex = pTexRow + g_depthMD.XOffset();
		for (XnUInt x = 0; x < g_depthMD.XRes(); ++x, ++pDepth, ++pTex)
		{
			if (*pDepth != 0 && *pDepth < 2000 )
			{
				int nHistValue = g_pDepthHist[*pDepth];
				pTex->nRed = nHistValue;
				pTex->nGreen = nHistValue;
				pTex->nBlue = nHistValue;
				
			}
			else
			{
				image.at<unsigned short>(y, x) = 0;
				pTex->nRed =0;
				pTex->nGreen =0;
				pTex->nBlue =0;
			}
		}

		pDepthRow += g_depthMD.XRes();
		pTexRow += g_nTexMapX;
	}
	const XnRGB24Pixel* pImageRow = g_imageMD.RGB24Data();
	XnRGB24Pixel* pTexRow1 = g_pTexMap1 + g_imageMD.YOffset() * g_nTexMapX;
	for (XnUInt y = 0; y < g_imageMD.YRes(); ++y)
	{
		const XnRGB24Pixel* pImage = pImageRow;
		XnRGB24Pixel* pTex = pTexRow1 + g_imageMD.XOffset();
		for (XnUInt x = 0; x < g_imageMD.XRes(); ++x, ++pImage, ++pTex)
		{
			*pTex = *pImage;
		}
		pImageRow += g_imageMD.XRes();
		pTexRow1 += g_nTexMapX;
	}
	Mat mtx,mtx1;
	const XnRGB24Pixel* pImageRow1 = g_pTexMap;
	const XnRGB24Pixel* pImageRow2 = g_pTexMap1;
	mtx=convertImageToCVImage(pImageRow1, g_imageMD);
	mtx1=convertImageToCVImage(pImageRow2, g_imageMD);
	for(int i=0;i<mtx.rows;i++){
		for(int j=0;j<mtx.cols;j++){
			if((mtx.at<Vec3b>(i,j)[0] != 0 || mtx.at<Vec3b>(i,j)[1] != 0 || mtx.at<Vec3b>(i,j)[2] != 0))
				mtx.at<Vec3b>(i,j)=mtx1.at<Vec3b>(i,j);
		}
	}
	
	imshow("MAT1",mtx1);
	
	return image;
}

Mat run_code_2(void)
{
	XnStatus rc = XN_STATUS_OK;
	// Read a new frame
	rc = g_context.WaitAnyUpdateAll();
	/*if (rc != XN_STATUS_OK)
	{
		printf("Read failed: %s\n", xnGetStatusString(rc));
		return;
	}*/
	Mat image(480,640,CV_8UC3);
	g_depth.GetAlternativeViewPointCap().SetViewPoint(g_image);
	g_depth.GetMetaData(g_depthMD);
	g_image.GetMetaData(g_imageMD);
	const XnDepthPixel* pDepth = g_depthMD.Data(); // this is the array of depth values
	const XnUInt8* pImage = g_imageMD.Data();
	unsigned int nImageScale = GL_WIN_SIZE_X / g_depthMD.FullXRes();
	// Calculate the accumulative histogram (the yellow display...)
	xnOSMemSet(g_pDepthHist, 0, MAX_DEPTH*sizeof(float));
	
	unsigned int nNumberOfPoints = 0;
	for (XnUInt y = 0; y < g_depthMD.YRes(); ++y)
	{
		for (XnUInt x = 0; x < g_depthMD.XRes(); ++x, ++pDepth)
		{
			if (*pDepth != 0)
			{
				uint16_t d = (uint16_t)(*pDepth);
				uint16_t val = t_gamma[d];
				uint8_t lb = val & 0xff;
				uint8_t ub = val >> 8;
				switch (ub) {
					case 0:
						image.at<Vec3b>(y,x)[2] = 255;
						image.at<Vec3b>(y,x)[1] = 255-lb;
						image.at<Vec3b>(y,x)[0] = 255-lb;
						break;
					case 1:	
						image.at<Vec3b>(y,x)[2] = 255;
						image.at<Vec3b>(y,x)[1] = lb;
						image.at<Vec3b>(y,x)[0] = 0;
						break;
					case 2:
						image.at<Vec3b>(y,x)[2] = 255-lb;
						image.at<Vec3b>(y,x)[1] = 255;
						image.at<Vec3b>(y,x)[0] = 0;
						break;
					case 3:
						image.at<Vec3b>(y,x)[2] = 0;
						image.at<Vec3b>(y,x)[1] = 255;
						image.at<Vec3b>(y,x)[0] = lb;
						break;
					case 4:
						image.at<Vec3b>(y,x)[2] = 0;
						image.at<Vec3b>(y,x)[1] = 255-lb;
						image.at<Vec3b>(y,x)[0] = 255;
						break;
					case 5:
						image.at<Vec3b>(y,x)[2] = 0;
						image.at<Vec3b>(y,x)[1] = 0;
						image.at<Vec3b>(y,x)[0] = 255-lb;
						break;
					default:
						image.at<Vec3b>(y,x)[2] = 0;
						image.at<Vec3b>(y,x)[1] = 0;
						image.at<Vec3b>(y,x)[0] = 0;
						break;
				}
				g_pDepthHist[*pDepth]++;
				nNumberOfPoints++;
			}
		}
	}
	for (int nIndex=1; nIndex<MAX_DEPTH; nIndex++)
	{
		g_pDepthHist[nIndex] += g_pDepthHist[nIndex-1];
	}
	if (nNumberOfPoints)
	{
		for (int nIndex=1; nIndex<MAX_DEPTH; nIndex++)
		{
			g_pDepthHist[nIndex] = (unsigned int)(256 * (1.0f - (g_pDepthHist[nIndex] / nNumberOfPoints)));
		}
	}

	xnOSMemSet(g_pTexMap, 0, g_nTexMapX*g_nTexMapY*sizeof(XnRGB24Pixel));
	const XnDepthPixel* pDepthRow = g_depthMD.Data();
	XnRGB24Pixel* pTexRow = g_pTexMap + g_depthMD.YOffset() * g_nTexMapX;
	
	pDepthRow = g_depthMD.Data();
	pTexRow = g_pTexMap + g_depthMD.YOffset() * g_nTexMapX;
	for (XnUInt y = 0; y < g_depthMD.YRes(); ++y)
	{
		const XnDepthPixel* pDepth = pDepthRow;
		XnRGB24Pixel* pTex = pTexRow + g_depthMD.XOffset();
		for (XnUInt x = 0; x < g_depthMD.XRes(); ++x, ++pDepth, ++pTex)
		{
			if (*pDepth != 0 && (*pDepth< 1500) && (*pDepth > threshold_value ))
			{
				int nHistValue = g_pDepthHist[*pDepth];
				pTex->nRed = nHistValue;
				pTex->nGreen = nHistValue;
				pTex->nBlue = nHistValue;
				
			}
			else
			{
				image.at<Vec3b>(y,x)[0]=0;
				image.at<Vec3b>(y,x)[1]=0;
				image.at<Vec3b>(y,x)[2]=0;
				pTex->nRed =0;
				pTex->nGreen =0;
				pTex->nBlue =0;
			}
		}

		pDepthRow += g_depthMD.XRes();
		pTexRow += g_nTexMapX;
	}
	const XnRGB24Pixel* pImageRow = g_imageMD.RGB24Data();
	XnRGB24Pixel* pTexRow1 = g_pTexMap1 + g_imageMD.YOffset() * g_nTexMapX;
	for (XnUInt y = 0; y < g_imageMD.YRes(); ++y)
	{
		const XnRGB24Pixel* pImage = pImageRow;
		XnRGB24Pixel* pTex = pTexRow1 + g_imageMD.XOffset();
		for (XnUInt x = 0; x < g_imageMD.XRes(); ++x, ++pImage, ++pTex)
		{
			*pTex = *pImage;
		}
		pImageRow += g_imageMD.XRes();
		pTexRow1 += g_nTexMapX;
	}
	Mat mtx,mtx1;
	const XnRGB24Pixel* pImageRow1 = g_pTexMap;
	const XnRGB24Pixel* pImageRow2 = g_pTexMap1;
	mtx=convertImageToCVImage(pImageRow1, g_imageMD);
	mtx1=convertImageToCVImage(pImageRow2, g_imageMD);
	for(int i=0;i<mtx.rows;i++){
		for(int j=0;j<mtx.cols;j++){
			if((mtx.at<Vec3b>(i,j)[0] != 0 || mtx.at<Vec3b>(i,j)[1] != 0 || mtx.at<Vec3b>(i,j)[2] != 0))
				mtx.at<Vec3b>(i,j)=mtx1.at<Vec3b>(i,j);
		}
	}
	
	imshow("MAT1",mtx1);
	
	return mtx1;
}
	
int main(int argc, char* argv[])
{
	ISoundEngine* engine = createIrrKlangDevice();
	
	if (!engine)
	{
		printf("Could not startup engine\n");
		return 0; // error starting up the engine
	}
	irrklang::ISoundSource* C4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoC4.ogg"); 
	irrklang::ISoundSource* D4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoD4.ogg");
	irrklang::ISoundSource* E4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoE4.ogg");
	irrklang::ISoundSource* F4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoF4.ogg"); 
	irrklang::ISoundSource* G4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoG4.ogg"); 
	irrklang::ISoundSource* A4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoA4.ogg");
	irrklang::ISoundSource* B4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoB4.ogg");
	irrklang::ISoundSource* C5 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoC5.ogg");
	irrklang::ISoundSource* Db4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoDb4.ogg");
	irrklang::ISoundSource* Eb4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoEb4.ogg");
	irrklang::ISoundSource* Gb4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoGb4.ogg");
	irrklang::ISoundSource* Ab4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoAb4.ogg");
	irrklang::ISoundSource* Bb4 = engine->addSoundSourceFromFile("/home/obiwan/Desktop/Piano project/Music files/Ogg files/PianoBb4.ogg");
	
	XnStatus rc;
	Rect black_keys[5];
	EnumerationErrors errors;
	
	int flag_prev = -1;
	int flag_now = -1;
	rc = g_context.InitFromXmlFile(SAMPLE_XML_PATH, g_scriptNode, &errors);
	if (rc == XN_STATUS_NO_NODE_PRESENT)
	{
		XnChar strError[1024];
		errors.ToString(strError, 1024);
		printf("%s\n", strError);
		return (rc);
	}
	else if (rc != XN_STATUS_OK)
	{
		printf("Open failed: %s\n", xnGetStatusString(rc));
		return (rc);
	}
	rc = g_context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_depth);
	if (rc != XN_STATUS_OK)
	{
		printf("No depth node exists! Check your XML.");
		return 1;
	}
	rc = g_context.FindExistingNode(XN_NODE_TYPE_IMAGE, g_image);
	if (rc != XN_STATUS_OK)
	{
		printf("No image node exists! Check your XML.");
		return 1;
	}
	g_depth.GetMetaData(g_depthMD);
	g_image.GetMetaData(g_imageMD);
	// Hybrid mode isn't supported in this sample
	if (g_imageMD.FullXRes() != g_depthMD.FullXRes() || g_imageMD.FullYRes() != g_depthMD.FullYRes())
	{
		printf ("The device depth and image resolution must be equal!\n");
		return 1;
	}
	if (g_imageMD.PixelFormat() != XN_PIXEL_FORMAT_RGB24)
	{
		printf("The device image format must be RGB24\n");
		return 1;
	}

	// Texture map init
	g_nTexMapX = (((unsigned short)(g_depthMD.FullXRes()-1) / 128) + 1) * 128;
	g_nTexMapY = (((unsigned short)(g_depthMD.FullYRes()-1) / 128) + 1) * 128;
	g_pTexMap = (XnRGB24Pixel*)malloc(g_nTexMapX * g_nTexMapY * sizeof(XnRGB24Pixel));
	g_pTexMap1 = (XnRGB24Pixel*)malloc(g_nTexMapX * g_nTexMapY * sizeof(XnRGB24Pixel));
	
	for(int i=0; i<2048; i++)
	{
		float v = i/2048.0;
		v = powf(v, 3)* 6;
		t_gamma[i] = v*6*256;
		inv_t_gamma[t_gamma[i]]=i;
	}
	
	Mat img = run_code_2();
	Mat img_rgb = img.clone();
	Mat img_rgb_2 = img.clone();
	Mat final;
	imshow("Image", img);
	Point2f p_t[4];
	p_t[0].x = 0;
	p_t[0].y = 0;
	p_t[1].x = img.cols -1;
	p_t[1].y = 0;
	p_t[2].x = img.cols -1;
	p_t[2].y = img.rows -1;
	p_t[3].x = 0;
	p_t[3].y = img.rows -1;
	
	setMouseCallback("Image", on_mouse, 0);
	waitKey(0);

	for(int i=0; i<4; i++)
	{
		cout<<"Here"<<endl;
		printf("Point %d: %f , %f\n", (i+1), p[i].x, p[i].y);
	}
	Mat mat_t = Mat :: zeros( img.rows/10, img.cols/10, img.type() );
	mat_t = getPerspectiveTransform(p, p_t);
	warpPerspective( img, final, mat_t, img.size() );
	Mat d_ = Mat(3, 3, CV_32FC1);
	for( int i=0; i<3; i++)
	{
		for( int j=0; j<3; j++)
		{
			d_.at<float>(i,j) = mat_t.at<double>(i,j);
		}
	}
	waitKey(0);
	
	img = final.clone();
	
	Mat img_gaussian, img2_gaussian;
	GaussianBlur( img, img_gaussian, Size(3, 3), 1);
	
	cvtColor(img_gaussian, img_grayscale, CV_BGR2GRAY);
	
	Mat img_thresh, img2_thresh;
	threshold(img_grayscale, img_thresh, 145, 255, THRESH_BINARY);

	Mat img_thresh_otsu;
	threshold(img_grayscale, img_thresh_otsu, 145, 255, THRESH_BINARY + THRESH_OTSU);
	
	Mat ker = getStructuringElement( MORPH_RECT, Size(7, 7), Point(1,1));
	Mat img_erode;
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	dilate(img_thresh_otsu, img_erode, ker);
	
	Mat img_xor;
	bitwise_xor(img_erode, img_thresh_otsu, img_xor);
	
	for(int i = img_xor.rows/3; i < img_xor.rows; i++){
		for( int j = 0; j < img_xor.cols; j++){
			img_xor.at<uchar>(i,j) = 0;
		}
	}
	
	printf("Processing 1\n");
	int array_x[100000], nos = 0; // array_x will store all the x values , nos will keep track of the number of such values entered
	int array_diff[100000]; // array_diff will store the differences in x values after sorting array_x;
	int array_x_final[100], nos_final = 0; // This array will store the final 
	//required values of x and nos_final will storre number of such x
	int threshold_x = 30; // This is the threshold on the difference of adjacent x values to determine the lines
	printf("Processing 2\n");
	for(int i = 0; i < img_xor.rows/2; i++){
		for( int j = 0; j < img_xor.cols; j++){
			if(img_xor.at<uchar>(i,j) == 255)
			{
				//printf("Here: %d\n", nos);
				array_x[nos++] = j;
			}
		}
	}
	printf("Processing 3\n");
	quickSort( array_x, 0, nos-1);
	
	int sum = array_x[0], num = 1;

	for( int i = 0; i < nos - 1; i++ )
	{
		array_diff[i] = array_x[i+1] - array_x[i];
	}	
	for( int i = 0; i < nos - 1; i++ )
	{
		if( array_diff[i] < threshold_x && array_diff[i] > -threshold_x )
		{
			sum += array_x[i+1];
			num ++;
		}
		else
		{
			array_x_final[nos_final++] = sum/num;
			sum = array_x[i+1];
			num = 1;
		}
		if( i == nos - 2)
		{
			array_x_final[nos_final++] = sum/num;
		}
	}
	printf("Processing 4\n");
	for(int i=0; i<nos_final; i++)
	{
		circle( img_xor, Point( array_x_final[i], 15 ), 6, Scalar(50), 10 );
	}
		
	imshow("XOR_ERODE", img_xor);
	printf("Processing 5\n");
	/* Processing the black keys */
	vector< vector< Point > > contours;
	for( int i = img_erode.rows -1; i > img_erode.rows -5; i --)
	{
		for( int j = 0; j < img_erode.cols; j++)
		{
			img_erode.at<uchar>(i, j) = 255;
		}
	}
	findContours( img_erode, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
	Mat clone_mat = img_erode.clone(), clone_mat2 = img2_thresh.clone();
	clone_mat.setTo(0);
	cout<<contours.size()<<endl;
	int index = 0;
	float area = contourArea(contours[0]);
	for( int i=1; i<contours.size(); i++)
	{
		if( area < contourArea(contours[i]) )
		{
			area = contourArea(contours[i]);
			index = i;
		}
	}
	int x_value[5];	
	for( int i=0; i<contours.size(); i++)
	{
		if( i != index ){
			black_keys[i] = boundingRect( contours[i]);
		}
	}
	for( int i=0; i<5; i++)
	{
		x_value[i] = black_keys[i].x;
	}
	quickSort(x_value, 0, 4);
	Rect black_keys_final[5];
	for( int k =0; k<5; k++)
	{
		int i=0;
		for(; i<5; i++)
		{
			if( x_value[k] == black_keys[i].x )
				break;
		}
		black_keys_final[k] = black_keys[i];
		rectangle( clone_mat, black_keys_final[k], Scalar(30*(k+1)), -1); 
	}
			
	imshow( "clone", clone_mat);
	Mat clone_mat_keypr = clone_mat.clone(); 
	waitKey(0);
	printf("Part 2\n");
	Mat img_up1;
	Mat img_p1 = run_code();
	Mat prev_frame=img_p1.clone();
	img_up1 = img_p1.clone();
	Mat img_up1_;
	printf("Error 1\n");
	int check_key = 1;
	int counter =0;
	Mat avg = img_p1.clone() / 20;
	
	for( int i=0; i<19; i++)
	{
		img_p1 = run_code();
		avg +=  img_p1/20;
	}
	Mat sub; 
	ker = getStructuringElement( MORPH_RECT, Size(3, 3), Point(1,1));
	Mat img_gray;
	int check = 1, i_pt = -1, j_pt = -1; 
	int i_new,j_new;
	int depth1 ; 
	int depth2 ;
	Mat b, d;	
	int check_black;
	while( check_key )
	{
		int check_press = 0;
		img_p1 = run_code();
		img_up1_ = (avg - img_p1);
		img_up1_.convertTo(sub, CV_8UC1, 1.0/256);
		
		erode(sub, sub, ker);
		erode(sub, sub, ker);
		
		imshow("sub",sub);
		threshold(sub, img_gray, 3, 255, THRESH_BINARY);
		erode(img_gray, img_gray, ker);
		erode(img_gray, img_gray, ker);
		erode(img_gray, img_gray, ker);
		
		check = 1;
		i_pt = -1;
		j_pt = -1; 
		for( int i=img_gray.rows -1; i> 0 /*1*img_gray.rows/5*/ && check; i --)
		{
			for( int j=0; j<img_gray.cols && check; j++)
			{
				if( img_gray.at<uchar>(i,j) == 255 )
				{
					check =0;
					i_pt = i;
					j_pt = j;
				}
			}
		}
		
		
		
		if( i_pt != -1 )
		{
			circle( img_gray, Point(j_pt, i_pt), 5, Scalar(127), 2);
			depth1 = img_up1.at<unsigned short>(i_pt, j_pt)/32; 
			depth2 = img_p1.at<unsigned short>(i_pt, j_pt)/32;	
			
			if( depth1 - depth2 < 80 && depth2 - depth1 < 80 && depth1 != 0 && depth2 != 0)
			{
				check_press =1;
				
				b = Mat(3, 1, CV_32FC1);
				b.at<float>(0, 0) = j_pt;
				b.at<float>(1, 0) = i_pt;
				b.at<float>(2, 0) = 1;
				d = d_ * b;
				i_new = int(d.at<float>(1, 0) / d.at<float>(2, 0));
				j_new = int(d.at<float>(0, 0) / d.at<float>(2, 0));
				
			}
			
		}

		warpPerspective( img_rgb, img, mat_t, img.size() );
		flag_now = flag_prev; 
		if( check_press && (img_p1.at<unsigned short>(i_pt, j_pt)/32 > 900) && (i_new > 0) && (j_new >0) && i_new < img.rows && j_new < img.cols )
		{
			circle( img, Point(j_new, i_new), 10, Scalar(255, 0, 255), 2);
			printf("CIRCLE DRAWN! : %d %d\n", i_new, j_new);
			check_black =0;
			int i=0;
			for( i=0; i<5; i++)
			{
				if( InOrOut(black_keys_final[i], Point(j_new, i_new) ))
				{
					rectangle( clone_mat_keypr, black_keys_final[i], Scalar(255), -1);
					check_black = 1;
					break;
				}
			}
			if(check_black)
			{
				if( i == 0)
				{
					//printf("C#!!\n");
					flag_now = 1;
					//	printf("Db4!!\n");
				}
				else if( i == 1)
				{
					//printf("D#!!\n");
					flag_now = 2;
					//	printf("Eb4!!\n");
				}
				else if( i == 2)
				{
					//printf("F#!!\n");
					flag_now = 3;
					//	printf("Gb4!!\n");
				}
				else if( i == 3)
				{
					//printf("G#!!\n");
					flag_now = 4;
					//	printf("Ab4!!\n");
				}
				else if( i == 4)
				{
					//printf("A#!!\n");
					flag_now = 5;
					//	printf("Bb4!!\n");
				}
					
			} 
			if(!check_black)
			{
				if( j_new <= array_x_final[0] )
				{
					// C4
					flag_now = 6;
				}
				if( j_new >= array_x_final[0] && j_new <=array_x_final[1])
				{
					//printf("D4!!\n");
					flag_now = 7;
				}
				else if( j_new >= array_x_final[1] && j_new <=array_x_final[2])
				{
					//printf("E4!!\n");
					flag_now = 8;
				}
				else if( j_new >= array_x_final[2] && j_new <=array_x_final[3])
				{
					//printf("F4!!\n");
					flag_now = 9;
				}
				else if( j_new >= array_x_final[3] && j_new <=array_x_final[4])
				{
					//printf("G4!!\n");
					flag_now = 10;
				}
				else if( j_new >= array_x_final[4] && j_new <=array_x_final[5])
				{
					//printf("A4!!\n");
					flag_now = 11;
				}
				else if( j_new >= array_x_final[5] && j_new <=array_x_final[6])
				{
					//printf("B4!!\n");
					flag_now = 12;
				}
				else if( j_new >= array_x_final[6] )
				{
					//printf("C5!!\n");
					flag_now = 13;
				}
			}
						
		}
		else
		{
			flag_now = -1;
			flag_prev = -1;
		}
		if( flag_now == -1 || flag_now == flag_prev )
		{
		}
		else
		{
			switch( flag_now )
			{
				case 1: //printf("Db4!!\n");
					engine->play2D(Db4);
					break;
				case 2: //printf("Eb4!!\n");
					engine->play2D(Eb4);
					break;
				case 3: //printf("Gb4!!\n");
					engine->play2D(Gb4);
					break;
				case 4: //printf("Ab4!!\n");
					engine->play2D(Ab4);
					break;
				case 5: //printf("Bb4!!\n");
					engine->play2D(Bb4);
					break;
				case 6: //printf("C4!!\n");
					engine->play2D(C4);
					break;
				case 7: //printf("D4!!\n");
					engine->play2D(D4);
					break;
				case 8: //printf("E4!!\n");
					engine->play2D(E4);
					break;
				case 9: //printf("F4!!\n");
					engine->play2D(F4);
					break;
				case 10: //printf("G4!!\n");
					engine->play2D(G4);
					break;
				case 11://printf("A4!!\n");
					engine->play2D(A4);
					break;
				case 12://printf("B4!!\n");
					engine->play2D(B4);
					break;
				case 13://printf("C5!!\n");
					engine->play2D(C5);
					break;
			}
		}	
		flag_prev = flag_now;
		check_press = 0;	
		
			
		imshow("Position", img);
		imshow("Keypr", clone_mat_keypr);
		clone_mat_keypr = clone_mat.clone();
		img_rgb = img_rgb_2.clone();	
		char c;
		c = waitKey(30);
		if(c == 27)
			check_key =0;
	}
	engine->drop(); // delete engine
	return 0;
}
